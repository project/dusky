<?php
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">
  <head>
    <?php print $head ?>
    <title><?php print $head_title ?></title>
    <?php print $styles ?>
    <?php print $scripts ?>
  </head>

<body>
<div id="headWrap">
 <div id="headpanel">
  <div id="logo">     
    <?php if (!empty($logo) || !empty($site_name)): ?>
      <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" alt="<?php print $site_name; ?>" rel="home">
        <?php if (!empty($logo)): ?> <img src='<?php print $logo; ?>' /> <?php endif; ?>
        <?php if (!empty($site_name)): ?> <span id="site-name"><?php print $site_name ?></span> <?php endif; ?>
      </a>
    <?php endif; ?>
  </div>
  <div id="menu">
    <?php if (!empty($primary_links)): ?>
      <?php print theme('links', $primary_links, array('class' => 'links primary-links')); ?>
    <?php endif; ?>
  </div>
 </div>
</div>

<div id="contentWrap">
 <div id="contentPanel">
  <div id="leftPanel">
   <?php print $left; ?>
  </div>
  <div id="middlePanel">
   <div class="tabs"><?php print $tabs ?><?php print $tabs2 ?></div>
   <?php print $content ?>
  </div>
</div>
</div>
<?php print $closure; ?>
</body>
</html>
